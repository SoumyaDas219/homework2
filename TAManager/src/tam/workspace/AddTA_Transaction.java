/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import jtps.jTPS_Transaction;
import tam.TAManagerApp;
import tam.data.TAData;
import tam.data.TeachingAssistant;

/**
 *
 * @author Soumya
 */
public class AddTA_Transaction implements jTPS_Transaction {
    
    private String taEmail;
    private String taName;
    private TAData data;
            
    public AddTA_Transaction(String name, String email, TAManagerApp initApp){
        taName = name;
        taEmail = email;
        data = (TAData)initApp.getDataComponent();
    }
    
    public void doTransaction() {
        data.addTA(taName, taEmail);
    }
    public void undoTransaction() {
        data.removeTA(taName);
    }
    
}
