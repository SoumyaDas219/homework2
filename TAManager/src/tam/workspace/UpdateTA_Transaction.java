/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import jtps.jTPS_Transaction;
import tam.TAManagerApp;
import tam.data.TAData;
import tam.data.TeachingAssistant;

/**
 *
 * @author Soumya
 */
public class UpdateTA_Transaction implements jTPS_Transaction {
    
    private String oldTAEmail;
    private String newTAEmail;
    private String oldTAName;
    private String newTAName;
    private TAData data;
    private TAController controller;
            
    public UpdateTA_Transaction(String oldName, String oldEmail, String newName, String newEmail, TAManagerApp initApp){
        oldTAEmail = oldEmail;
        newTAEmail = newEmail;
        oldTAName = oldName;
        newTAName = newName;
        data = (TAData)initApp.getDataComponent();
        controller = new TAController(initApp);
    }
    
    public void doTransaction() {
        data.removeTA(oldTAName);
        data.addTA(newTAName, newTAEmail);
        controller.updateTAInOfficeHoursGrid(oldTAName, newTAName);
    }
    public void undoTransaction() {
        data.removeTA(newTAName);
        data.addTA(oldTAName, oldTAEmail);
        controller.updateTAInOfficeHoursGrid(newTAName, oldTAName);
    }
    
}
