/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import jtps.jTPS_Transaction;
import tam.TAManagerApp;
import tam.data.TAData;
import tam.data.TeachingAssistant;

/**
 *
 * @author Soumya
 */
public class CellToggle_Transaction implements jTPS_Transaction {
    
    private String taEmail;
    private String taName;
    private String Key;
    private TAData data;
            
    public CellToggle_Transaction(String name, String email, String key, TAManagerApp initApp){
        taName = name;
        taEmail = email;
        Key = key;
        data = (TAData)initApp.getDataComponent();
    }
    
    public void doTransaction() {
       data.toggleTAOfficeHours(Key, taName);
    }
    public void undoTransaction() {
       data.toggleTAOfficeHours(Key, taName);
    }
    
}
